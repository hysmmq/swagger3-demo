package com.swagger3demo.controller.two;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mq
 * @ClassName HelloWorldController.java
 * @Description
 * @createTime 2022年09月01日 13:23:00
 */

@Api(tags = "HelloWorld类测试2")
@RestController
public class HelloWorldController2 {

    @ApiOperation("测试方法2")
    @GetMapping("/helloWorld2")
    public String helloWorld(){
        return "helloWorld2";
    }


}
