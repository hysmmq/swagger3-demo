package com.swagger3demo.controller.one;

import com.swagger3demo.entity.User;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mq
 * @ClassName HelloWorldController.java
 * @Description
 * @createTime 2022年09月01日 13:23:00
 */

@Api(tags = "HelloWorld类测试")
@RestController
public class HelloWorldController {

    @ApiOperation("测试方法")
    @GetMapping("/helloWorld")
    public String helloWorld(){
        return "helloWorld";
    }

    /**
     * 查询
     * @param name
     * @param age
     * @return
     */
    @PostMapping("/search")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name",value = "姓名",required = true,paramType = "query"),
            @ApiImplicitParam(name = "age",value = "年龄",required = true,paramType = "query",dataType = "Integer")

    })
    @ApiOperation("测试查询")
    public String search(String name,Integer age){
        return name + ":" +age;
    }


    /**
     * 测试添加
     * @param user
     * @return
     */
    @ApiOperation("测试添加")
    @PostMapping("/add")
    public String add(User user){
        return user.getName() + ":" + user.getAge();
    }


    @GetMapping("/user/{id}")
    @ApiOperation("根据id获取用户信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "id",value = "用户编号",required = true,paramType = "path")})
    @ApiResponses({
            @ApiResponse(code = 408,message = "指定业务的报错信息，返回客户端"),
            @ApiResponse(code = 400,message = "请求参数没填好"),
            @ApiResponse(code = 404,message = "请求路径没有或网页跳转路径不对")
    })
    public User load(@PathVariable("id") Integer id){
        return new User(id,"jack",32);
    }

}
