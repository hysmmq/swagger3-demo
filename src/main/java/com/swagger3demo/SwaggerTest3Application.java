package com.swagger3demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author mq
 * @ClassName SwaggerTest3Application.java
 * @Description
 * @createTime 2022年09月01日 13:21:00
 */

@EnableOpenApi
@SpringBootApplication
public class SwaggerTest3Application {

    public static void main(String args[]){
        SpringApplication.run(SwaggerTest3Application.class);
    }

}
